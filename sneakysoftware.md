Asterisk (*) signifies that I consider the software unmatched for my usage.

**GNU/Linux OS**
- Void Linux (musl)
- Alpine Linux

**Init System**
- runit
- openrc

**Login Manager**
- Ly

**Video Player**
- mpv*

**Terminal Emulator**
- st

**Browser**
- FireFox* (or its derivatives)
- surf (if extenstions aren't a priority)

**Music Player**
- mpg123

**File Manager**
- lf (commandline)
- pcmanfm-qt (graphical)

**Image Viewer /  Wallpaper Setter**
- Feh

**Window Manager**
- dwm (tiling)
- openbox (stacking)

**Document Viewer**
- Zathura* (+mupdf backend, djvu support)

**Calculator**
- GNU bc

**text editor**
- neovim* (also a nice IDE)
- micro*
- mousepad (small graphical editor)

**Dictionary**
- dictd
- espeak (for word pronunciation) (optimally bind to key)

**Other sneaky utilities**
- aria2* (multi-protocol downloader, supports torrent as well)
- yt-dlp* (downloader when they don't want you to)

**Screenshot/Screengrab**
- scrot (screenshot) (optimally bind to a key)
- ffmpeg (screencast)

**Others**
not necessary for everybody, but in what they do they are best
- gimp (image editor)
- audacity (audio editor)
- shotcut (video editor)
- LibreOffice
- OBS Studio (recorder/streamer) [ffmpeg is enough for basic recording though]
- guvcview (webcam) (ffmpeg does this too, but this is a gui)
